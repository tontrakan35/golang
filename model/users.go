package User

type All struct {
	Prefix     string `json:"prefix"`
	Prefixcode string `json:"prefixCode"`
	Name       string `json:"name"`
	Lastname   string `json:"lastName"`
	Username   string `json:"userName"`
	Password   string `json:"password"`
	Email      string `json:"email"`
	Position   string `json:"position"`
	Status     string `json:"status"`
	School     string `json:"school"`
	Imgprofile string `json:"imgProfile"`
	Imgbg      string `json:"imgBg"`
	Usercreate string `json:"userCreate"`
	Createdt   string `json:"createDt"`
	Userupdate string `json:"userUpdate"`
	Updatedt   string `json:"updateDt"`
}
