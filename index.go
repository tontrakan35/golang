package main

import (
	"golang/routers"
	models "golang/service"
	"golang/setting"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

var app *gin.Engine

func init() {
	setting.Setup()
	models.Setup()
	app = routers.Setup()
}

func main() {
	// router.Run(":80")
	server := &http.Server{
		Addr:           ":80",
		Handler:        app,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	log.Printf("[info] start http server listening %s", server.Addr)
	server.ListenAndServe()
}
