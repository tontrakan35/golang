package setting

import (
	"log"

	"github.com/go-ini/ini"
)

type Database struct {
	Type        string
	User        string
	Password    string
	Host        string
	TablePrefix string
}

var cfg *ini.File
var DB = &Database{}

func Setup() {
	var err error
	cfg, err = ini.Load("config/app.ini")
	if err != nil {
		log.Fatalf("setting.Setup, fail to parse 'conf/app.ini': %v", err)
	}

	mapTo("database", DB)
}

func mapTo(section string, variable interface{}) {
	err := cfg.Section(section).MapTo(variable)
	if err != nil {
		log.Fatalf("Cfg.MapTo %s err: %v", section, err)
	}
}
