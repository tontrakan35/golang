package countries

import (
	models "golang/service"

	"github.com/gin-gonic/gin"
)

type Countries struct {
	CodeId    int `gorm:"primary_key"`
	Code      string
	ShortName string
	LongName  string
}

func View(c *gin.Context) {
	var result []Countries
	db := models.Setup()
	db.Table("countrys").Order("longName").Find(&result)
	c.JSON(200, gin.H{"response": result, "length": len(result)})
}
