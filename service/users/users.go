package users

import (
	User "golang/model"
	models "golang/service"

	"github.com/gin-gonic/gin"
)

type Insert struct {
	ApiRequest User.All `json:"apiRequest"`
}

func Register(c *gin.Context) {
	db := models.Setup()
	var item Insert
	c.BindJSON(&item)
	err := db.Table("users").Create(item.ApiRequest).Error
	c.JSON(200, err)
}
