package models

import (
	"fmt"
	"golang/setting"
	"log"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func Setup() *gorm.DB {
	var cfg string
	cfg = fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", setting.DB.User, setting.DB.Password, setting.DB.Host, setting.DB.TablePrefix)
	db, err := gorm.Open(mysql.Open(cfg), &gorm.Config{})
	if err != nil {
		log.Fatalf("models.Setup err: %v", err)
	}
	return db
}
