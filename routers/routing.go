package routers

import (
	"golang/routers/api/v1/auth"
	"golang/routers/api/v1/global"

	"github.com/gin-gonic/gin"
)

func Setup() *gin.Engine {
	routers := gin.Default()
	global.Setup(routers)
	auth.Setup(routers)
	return routers
}
