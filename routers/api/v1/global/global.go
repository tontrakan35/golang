package global

import (
	users "golang/service/users"

	"github.com/gin-gonic/gin"
)

func Setup(r *gin.Engine) {
	r.POST("api/v1/register", users.Register)
}
