package auth

import (
	authenticate "golang/middleware"
	"golang/service/countries"

	"github.com/gin-gonic/gin"
)

func Setup(r *gin.Engine) {
	auth := r.Group("api/v1/auth")
	auth.Use(authenticate.JWTAuthMiddleware()).GET("/countries", countries.View)
}
